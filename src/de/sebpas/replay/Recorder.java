package de.sebpas.replay;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.sebpas.api.util.mojang.OnlineUUIDFetcher;
import de.sebpas.replay.event.RecordingStartEvent;
import de.sebpas.replay.event.RecordingStoppedEvent;
import de.sebpas.replay.filesystem.Frame;
import de.sebpas.replay.filesystem.FrameAction;
import de.sebpas.replay.filesystem.serializable.SerializedGameProfile;
import de.sebpas.replay.filesystem.serializable.SerializedLocation;
import de.sebpas.replay.recorder.listener.InteractListener;
import de.sebpas.replay.recorder.listener.MoveListener;
import de.sebpas.replay.recorder.listener.SpawnDespawnListener;

public class Recorder{
	private ReplaySystem plugin;
	private boolean isRecording = false;
	
	private volatile Frame currentFrame;
	private boolean changed;
	
	private Runnable runnable = new Runnable() {
		
		@Override
		public void run() {
			if(Bukkit.getOnlinePlayers().size() != 0 && isRecording){
				synchronized (currentFrame) {
					if(changed){
						try {
							synchronized (currentFrame) {
								plugin.getFileManager().appendFrame(currentFrame);
							}
						} catch (IOException e) {}
					}
					plugin.addTick();
					currentFrame = new Frame(plugin.getHandledTicks());
					changed = false;
				}
			}
			
		}
	};
	
	public Recorder(ReplaySystem plugin){
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(new MoveListener(plugin), plugin);
		Bukkit.getPluginManager().registerEvents(new InteractListener(plugin), plugin);
		Bukkit.getPluginManager().registerEvents(new SpawnDespawnListener(plugin), plugin);
		Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, runnable, 1L, 1L);
	}
	
	/** starts capturing */
	public synchronized void recorde(){
		Bukkit.getPluginManager().callEvent(new RecordingStartEvent());
		this.isRecording = true;
		currentFrame = new Frame(plugin.getHandledTicks());
		for(Player p : Bukkit.getOnlinePlayers()){
			FrameAction action = new FrameAction(new SerializedGameProfile(OnlineUUIDFetcher.getInstance().getUUID(p.getName()), p.getName()));
			action.setLocation(new SerializedLocation(p.getLocation().clone()));
			addAction(action);
		}
	}
	
	/** stops capturing */
	public synchronized void stop(){
		Bukkit.getPluginManager().callEvent(new RecordingStoppedEvent());
		this.isRecording = false;
	}
	
	public boolean isRecording(){
		return this.isRecording;
	}
	
	public synchronized void addAction(FrameAction action){
		this.changed = true;
		this.currentFrame.addAction(action);
	}
}
