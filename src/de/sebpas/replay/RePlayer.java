package de.sebpas.replay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import de.sebpas.replay.event.ReplayStartEvent;
import de.sebpas.replay.filesystem.Frame;
import de.sebpas.replay.filesystem.FrameAction;
import de.sebpas.replay.filesystem.ZipFile;
import de.sebpas.replay.filesystem.serializable.ProjectileType;
import de.sebpas.replay.filesystem.serializable.SerializedProjectile;
import de.sebpas.replay.npc.NPC;
import de.sebpas.replay.util.PlayingPlayer;

public class RePlayer {
	/** the amout of ticks saved in this replay */
	private long maxTicks = -1;

	private List<World> loadedWorlds;
	
	private volatile double lastTick;
	private volatile Map<Player, PlayingPlayer> players;
	private volatile double currentTick;
	private volatile double velocity = 1;
	private boolean isRunning = false;
	
	
	/** which ticks were already scanned */
	private Object lastPacket;
	private InputStream in;
	private ZipFile file;
	
	/** NPC stuff */
	private List<NPC> npcs = new ArrayList<NPC>();
	
	/** thread stuff */
	private BukkitTask bukkitTask;
	
	private volatile boolean isSkipPressed;
	private int nextReset = 0;
	
	/**
	 * the task which is executed when playing a replay file
	 */
	
	private Runnable task = new Runnable() {
		
		@Override
		public void run() {
			if(isRunning || isSkipPressed){
				double ticks = Math.floor(currentTick - lastTick);
				/*@DEBUG*/ System.out.println((isSkipPressed ? "[Skipping]" : "") + "Last tick: " + lastTick + ", current: " + currentTick + ", ticks to run: " + ticks + ", running with velocity " + velocity);
				if(ticks >= 0){
					for(int j = 0; j < ticks; j++){
						/*@DEBUG*/ System.out.println("running: " + j  + " / " + ticks + " ticks!");
						try {
							RePlayer.this.handleFrame(j);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						lastTick = Math.floor(currentTick);
					}
				}else{
					for(int j = (int) ticks; j < 0; j++){
						/*@DEBUG*/ System.out.println("running: " + j  + " / " + ticks + " ticks!");
						try {
							RePlayer.this.handleFrame(j);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						lastTick = Math.round(currentTick);
					}
				}
			
				/** tick increase */
				System.out.println("Added: " + currentTick + " + " + velocity + " = " + (currentTick + velocity));
				currentTick += velocity;
				
				if(isSkipPressed){
					System.out.println(nextReset + " times to reset");
					nextReset ++;
					if(nextReset >= 5){
						isSkipPressed = false;
						velocity = 1;
						nextReset = 0;
					}
				}
			}
			
			for(Player p : players.keySet()){
				p.setExp((float) currentTick / (float) getLastTick());
				p.setLevel((int) Math.round(currentTick / 20));
				p.setHealth(20D);
				p.setFoodLevel(20);
			}
			
			if(currentTick > getLastTick() || players.isEmpty())
				stop();

		}
	};
	
	private synchronized void handleAction(final FrameAction action){
		if(action.getGameProfile() != null){
			NPC npc = null;
			
			for(NPC ncp : npcs){ // ncp means in this case not nocheatplus ;D
				if(ncp.getName().equals(action.getGameProfile().getName())){
					npc = ncp;
				}
			}
			
			if(npc == null){
				npc = new NPC(action.getGameProfile().getUuid().toString(), action.getGameProfile().getName(), action.getLocation(this));
				npcs.add(npc);
				npc.spawn();
			}else{
				if(action.isDead()){
					npc.deSpawn();
				}
				
				if(action.isSpawn()){
					npc.spawn();
				}
				
				if(action.isMoving()){
					float yaw = action.getLocation(this).getYaw(), pitch = action.getLocation(this).getPitch();

					if(!action.isSneaking() && !action.isSprinting() && !action.isBlocking())
						npc.resetMovement();

						if(action.isSprinting()){
							npc.sprint();
						}
						if(action.isSneaking()){
							npc.sneak();
						}
						if(action.isBlocking()){
							npc.block();
						}

					if(action.getLocation(this) != null)
						npc.teleport(action.getLocation(this));
					if(!action.isMovedByKnockBack())
						npc.look(yaw, pitch);
				}
				if(action.isSneaking())
					npc.sneak();
				
				if(action.isSprinting())
					npc.sprint();
				
				if(action.isBlocking())
					npc.block();
				
				if(action.isSwinging())
					npc.swingArm();
				
				if(action.isDamaged())
					npc.damageAnimation();
				
				if(action.getHelmet() != null)
					npc.setHelmet(action.getHelmet());
				
				if(action.getChestPlate() != null)
					npc.setChestplate(action.getChestPlate());
				
				if(action.getLeggings() != null)
					npc.setLeggings(action.getLeggings());
				
				if(action.getBoots() != null)
					npc.setBoots(action.getBoots());
				
				if(action.getIteminhand() != null){
					npc.setItemInHand(action.getIteminhand());
				}
				
				if(action.getChatMessages() != null){
					for(Player chattarget : players.keySet()){
						chattarget.sendMessage(action.getChatMessages());
					}
				}
			}
		}else{
			if(action.getProjectiles() != null){
				for(final SerializedProjectile p : action.getProjectiles()){
					new BukkitRunnable(){
						@Override
						public void run() {
							Entity e = null;
							Location eye = action.getLocation(RePlayer.this).clone();
							if(p.getType().equals(ProjectileType.ARROW)){
								e = eye.getWorld().spawn(eye, Arrow.class);
							}
							
							if(p.getType().equals(ProjectileType.EGG)){
								e = eye.getWorld().spawn(eye, Egg.class);
							}
							
							if(p.getType().equals(ProjectileType.ENDERPEARL)){
								return;
							}
							
							if(p.getType().equals(ProjectileType.EXPBOTTLE)){
								e = eye.getWorld().spawn(eye, ThrownExpBottle.class);
							}

							if(p.getType().equals(ProjectileType.POTION)){
								e = eye.getWorld().spawn(eye, ThrownPotion.class);
								ThrownPotion potion = (ThrownPotion) e;
								potion.setItem(p.getPotionItem().unbox());
							}
							
							if(p.getType().equals(ProjectileType.SNOWBALL)){
								e = eye.getWorld().spawn(eye, Snowball.class);
							}
							e.setVelocity(new Vector(p.getX(), p.getY(), p.getZ()));
						}
					}.runTask(ReplaySystem.getInstance());
				}
			}
			
			if(action.getDespawnedItems() != null){
				new BukkitRunnable() {
					
					@Override
					public void run() {
						for(ItemStack i : action.getDespawnedItems()){
							for(Item t : action.getLocation(RePlayer.this).getWorld().getEntitiesByClass(Item.class)){
								if(t.getItemStack().equals(i))
									t.remove();
							}
						}
					}
				}.runTask(ReplaySystem.getInstance());
			}
			
			if(action.getThrowables() != null){
				new BukkitRunnable() {
					
					@Override
					public void run() {
						for(ItemStack i : action.getThrowables()){
							org.bukkit.Location eyes = action.getLocation(RePlayer.this).clone().add(0, 1.62D, 0);
							Item drop = eyes.getWorld().dropItemNaturally(eyes, i);
							drop.setVelocity(action.getLocation(RePlayer.this).getDirection().normalize().multiply(0.2D));
						}
					}
				}.runTask(ReplaySystem.getInstance());
			}
		}
	}

	public String getId(){
		return this.file.getName().replace(".rpl", "");
	}

	/**
	 * handles a single frame j
	 * @param j
	 */
	private synchronized void handleFrame(int j){
		if(lastTick + j < 0){
			return;
		}
		for(Frame frame: readTicks((int) lastTick + j)){
			for(FrameAction action : frame.getActions()){
				this.handleAction(action);
			}
		}

	}
	
	public void pause(){
		this.isRunning = false;
	}
	
	public void continueReplay(){
		this.isRunning = true;
	}
	
	public RePlayer(final ZipFile in, Player player){
		if(in == null){
			ReplaySystem.sendBroadcastError("Could not play replay: File not found!");
			return;
		}
		try {
			this.in = in.getReplayFile();
			this.file = in;
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.getLastTick();
		
		this.currentTick = 0;
		this.players = new HashMap<Player, PlayingPlayer>();
		this.players.put(player, new PlayingPlayer(player));
		ReplaySystem.getInstance().addPlayer(this);
		this.loadedWorlds = new ArrayList<World>();
		new BukkitRunnable(){

			public void run() {
				for(String s : in.unboxWorlds()){
					World world = Bukkit.createWorld(new WorldCreator("replay-" + getId() + "-" + s));
					if(world != null)
						RePlayer.this.loadedWorlds.add(world);
				}
			}
		}.runTask(ReplaySystem.getInstance());
	}
	
	@Deprecated
	public RePlayer(final ZipFile in, Map<Player, PlayingPlayer> players) throws FileNotFoundException{
		if(in == null){
			ReplaySystem.sendBroadcastError("Could not play replay: File not found!");
			return;
		}
		try {
			this.in = in.getReplayFile();
			this.file = in;
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.getLastTick();
		
		this.currentTick = 0;
		this.players = players;
		ReplaySystem.getInstance().addPlayer(this);
		this.loadedWorlds = new ArrayList<World>();
		new BukkitRunnable(){

			public void run() {
				for(String s : in.unboxWorlds()){
					World world = Bukkit.createWorld(new WorldCreator("replay-" + getId() + "-" + s));
					if(world != null)
						RePlayer.this.loadedWorlds.add(world);
				}
			}
		}.runTask(ReplaySystem.getInstance());
	}

	/**
	 * updates the npc's locations. Used when a player teleports using the teleporter while playing a replay file 
	 * @param p
	 */
	public void updateLocation(Player p){
		for(NPC npc : npcs){
			if(npc.getLocation().getWorld().getName().equals(p.getLocation().getWorld().getName())){
				npc.spawn();
			}
		}
	}

	/**
	 * starts playing (only use when starting the replay; not used after paused it)
	 */
	public void start(){
		this.isRunning = true;
		Bukkit.getPluginManager().callEvent(new ReplayStartEvent(this));
		
		for(Player p : this.players.keySet()){
			p.setGameMode(GameMode.ADVENTURE);
			p.setAllowFlight(true);
			p.setFlying(true);
			p.setHealth(20D);
			p.setFoodLevel(20);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', ReplaySystem.getInstance().getPrefix() + "&3Replay gestartet!"));	
		}
		bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(ReplaySystem.getInstance(), task, 1L, 1L);
	}
	
	/**
	 * cancels the tasks and stops the replay
	 */
	public void stop(){
		this.isRunning = false;
		bukkitTask.cancel();
		for(NPC n : npcs)
			n.deSpawn();
		for(Player p : this.players.keySet()){
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
			this.getPlayers().get(p).throwIntoGame(p, true);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', ReplaySystem.getInstance().getPrefix() + " &3Das Replay ist beendet. Du hast das Replay verlassen."));
		}

		for(World world : loadedWorlds){
			Bukkit.unloadWorld(world, false);
		}

		new BukkitRunnable(){

			public void run() {
				for(World world : loadedWorlds){
					try {
						RePlayer.this.delete(new File(world.getName()));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.runTaskLater(ReplaySystem.getInstance(), 20);
		ReplaySystem.getInstance().onPlayerStopped(this);
	}

	/**
	 * deletes a folder and its subfiles
	 * @param file
	 * @throws IOException
	 */
	private void delete(File file) throws IOException {
		File[] children = file.listFiles();
		if (children != null) {
			for (File child : children) {
				delete(child);
			}
		}
		Files.deleteIfExists(file.toPath());
	}
	
	/**
	 * ONLY use while stopping plugin
	 */
	public void stopWithoutTask(){
		this.isRunning = false;
		bukkitTask.cancel();
		for(NPC n : npcs)
			n.deSpawn();
		for(Player p : this.players.keySet()){
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
			this.getPlayers().get(p).throwIntoGame(p, false);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', ReplaySystem.getInstance().getPrefix() + " &3Das Replay ist beendet. Du hast das Replay verlassen."));
		}
		new BukkitRunnable(){

			public void run() {
				for(World world : loadedWorlds){
					Bukkit.unloadWorld(world, false);
				}
				for(World world : loadedWorlds){
					try {
						RePlayer.this.delete(new File(world.getName()));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.runTaskLater(ReplaySystem.getInstance(), 5);
		ReplaySystem.getInstance().onPlayerStopped(this);
	}
	
	@Deprecated
	/**
	 * use RePlayer.stop() instead
	 * @param p
	 */
	public void removePlayer(Player p){
		this.players.remove(p);
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', ReplaySystem.getInstance().getPrefix() + " &3Du hast das Replay verlassen."));
		this.stop();
	}
	
	public double getVelocity() {
		return velocity;
	}
	
	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}
	
	/**
	 * @param velocity
	 * @param isSkipPressed doesnt matter because it will be set automatiocally to true :p
	 */
	public void setVelocity(double velocity, boolean isSkipPressed) {
		synchronized (this) {
			if(!this.isSkipPressed){
				this.velocity = velocity;
				this.isSkipPressed = true;
			}
		}
	}
	
	public synchronized double getCurrentTick() {
		return currentTick;
	}
	
	public boolean setCurrentTick(double currentTick) {
		if(currentTick >= this.getLastTick() || currentTick < 0)
			return false;
		this.currentTick = currentTick;
		this.lastTick = currentTick -= velocity;
		return true;
	}
	
	public Map<Player, PlayingPlayer> getPlayers() {
		return players;
	}
	
	public boolean isRunning() {
		return isRunning;
	}
	
	/**
	 * @return the max amount of ticks saved in this replay
	 */
	private long getLastTick(){
		if(maxTicks != -1){
			return maxTicks;
		}
		maxTicks = file.getMaxTicks();
		return -1;
	}
	
	private ObjectInputStream object;
	
	/**
	 * reads all lines with the current tick
	 * @param tick
	 * @return list of all strings with tick <i>tick</i>
	 * @throws IOException 
	 */
	private synchronized List<Frame> readTicks(int tick){
		List<Frame> rtn = new ArrayList<Frame>();
		
		if(object == null){
			try {
				in.close();
				in = file.getReplayFile();
				object = new ObjectInputStream(in);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(lastPacket != null){
			if(lastPacket instanceof Frame){
				Frame frame = (Frame) lastPacket;
				
				if(frame.getTick() > tick && (isSkipPressed ? velocity > 0 : true)){
					return rtn;
				}
				rtn.add(frame);
			}
		}
		
		try {
			while((lastPacket = object.readObject()) != null){
				if(lastPacket != null){
					if(!(lastPacket instanceof Frame)){
						object.reset();
						continue;
					}
					Frame frame = (Frame) lastPacket;
					if(isSkipPressed){
						System.out.println(frame.getTick() + "<" + tick);
						if(frame.getTick() < tick){
							continue;
						}
						
						if(frame.getTick() > tick){
							object = null;
							return rtn; 
						}
					}else{
						if(frame.getTick() > tick){
							break;
						}
						
						if(frame.getTick() < tick){
							continue;
						}
					}
					rtn.add(frame);
					if(object.markSupported())
						object.reset();
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage() == "" ? e.getLocalizedMessage() : e.getMessage());
//			/* @DEBUG */ e.printStackTrace(); 
		}
		
		return rtn;
	}

	public NPC getNPCByName(String name){
		for(NPC n : npcs){
			if(n.getName().equalsIgnoreCase(name))
				return n;
		}
		return null;
	}

	public List<NPC> getNpcs() {
		return npcs;
	}
}
