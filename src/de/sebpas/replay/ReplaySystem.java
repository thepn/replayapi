package de.sebpas.replay;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.sebpas.replay.command.CommandReplay;
import de.sebpas.replay.event.ReplayStoppedEvent;
import de.sebpas.replay.filesystem.FileManager;
import de.sebpas.replay.recorder.listener.PacketListener;
import de.sebpas.replay.util.PlayingPlayer;

public class ReplaySystem extends JavaPlugin{
	private long ranTicks = 0;
	private FileManager fileSystem;
	private Recorder recorder;
	
	private static ReplaySystem instance = null;
	
	/** list of all replayers */
	private List<RePlayer> replayers = new ArrayList<RePlayer>();
	
	/** plugin prefixes */
	private static String prefix = "&8[&3Replay&8]: &r";
	private static String error = "&8[&cReplay&8]: &c";
	
	/** if true the recorder will start after loading the plugin (may throw nullpointerexception while doing this in onEnable of other plugins */
	private static boolean startWhenEnabled;
	
	/**
	 * Bukkit methods
	 */
	
	@Override
	public void onEnable() {
		instance = this;
		System.out.println("[Replay]: Enabled!");
		this.fileSystem = new FileManager();
		this.recorder = new Recorder(this);
		
		this.getCommand("replay").setExecutor(new CommandReplay(this));

		this.getServer().getPluginManager().registerEvents(new PlayingPlayer(), this);
		
		PacketListener.listenToPackets(this);
		
		if(startWhenEnabled)
			this.start();
		
		File dir = new File("plugins/Replays");
		if(!dir.exists())
			dir.mkdirs();
	}
	@Override
	public void onDisable() {
		if(recorder.isRecording()){
			this.stop();
		}
		for(RePlayer r : replayers)
			r.stopWithoutTask();
	}
	
	/** returns a new instance of this main class */
	public static ReplaySystem getInstance(){
		return instance;
	}
	
	
	//TODO throw exception
	public void start(){
		if(getRecorder().isRecording())
			return;
		this.ranTicks = 0;
		this.fileSystem.reset();
		this.fileSystem.createNewFile();
		this.recorder.recorde();
	}
	
	public void stop(){
		if(!recorder.isRecording()){
			return;
		}
		this.recorder.stop();
		fileSystem.saveReplayFile();
		this.ranTicks = 0;
	}
	
	/** returns the amount of recorded ticks */
	public long getHandledTicks(){
		return this.ranTicks;
	}
	
	/** sends a message to all players and the console */
	public static void sendBroadcast(String msg){
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
	}
	
	/** sends an error message to all players and the console */
	public static void sendBroadcastError(String msg){
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', error + msg));
	}
	public String getPrefix(){
		return prefix;
	}
	public String getErrorPrefix(){
		return error;
	}
	
	/** the tick counter */
	public void addTick(){
		++ ranTicks;
		if(ranTicks >= 20 * 60 * 60){
			this.stop();
			this.start();
		}
	}
	
	/** returns the filemanager */
	public FileManager getFileManager(){
		return fileSystem;
	}
	
	/** returns the recording thread */
	public Recorder getRecorder(){
		return this.recorder;
	}
	/** returns true if the player is already watching a replay */
	public boolean isAlreadyInReplay(Player p){
		for(RePlayer r : replayers)
			if(r.getPlayers().containsKey(p))
				return true;
		return false;
	}
	
	public void addPlayer(RePlayer p){
		this.replayers.add(p);
	}
	
	public void onPlayerStopped(RePlayer p){
		this.replayers.remove(p);
		synchronized (p) {
			this.getServer().getPluginManager().callEvent(new ReplayStoppedEvent(p));
		}
	}
	public RePlayer getPlayersRePlayer(Player p){
		for(RePlayer r : replayers){
			if(r.getPlayers().containsKey(p))
				return r;
		}
		return null;
	}
	public RePlayer getPlayersRePlayer(HumanEntity p){
		for(RePlayer r : replayers){
			for(Player t : r.getPlayers().keySet())
				if(t.getName().equals(p.getName()))
					return r;
		}
		return null;
	}
	/** starts the recording while loading the plugin */
	public static void startAfterEnabling(){
		startWhenEnabled = true;
	}
}
