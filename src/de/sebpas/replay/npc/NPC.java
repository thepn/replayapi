package de.sebpas.replay.npc;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.comphenix.packetwrapper.WrapperPlayServerAnimation;
import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerEntityEquipment;
import com.comphenix.packetwrapper.WrapperPlayServerEntityHeadRotation;
import com.comphenix.packetwrapper.WrapperPlayServerEntityLook;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.packetwrapper.WrapperPlayServerRelEntityMoveLook;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.NativeGameMode;
import com.comphenix.protocol.wrappers.EnumWrappers.PlayerInfoAction;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;

import de.sebpas.api.util.PacketUtil;
import de.sebpas.api.util.mojang.GameProfileFetcher;
import de.sebpas.replay.ReplaySystem;

public class NPC {
	private boolean isAlive;
	
	private int ID;
	private Location location;
	private GameProfile profile;
	private String name;
	private Player player;
	
	/** movement */
	private boolean isSneaking = false;
	private boolean isSprinting = false;
	private boolean isBlocking = false;
	
	/** items (armor, item in hand etc) */
	private ItemStack helmet;
	private ItemStack chestplate;
	private ItemStack leggins;
	private ItemStack boots;
	private ItemStack handHeld;
	
	/**
	 * @param name
	 * @param loc
	 */
	public NPC(String uuid, String name, Location loc){
		this.ID = (int) Math.ceil(Math.random() * 1000) + 2000;
		try {
			profile = GameProfileFetcher.fetch(UUID.fromString(uuid));
		} catch (IOException e) {
			profile = new GameProfile(UUID.fromString(uuid), name);
		}
		this.location = loc.clone();
		this.name = name;
	}
	
	public NPC(String uuid, String name, Location loc, Player player){
		this.ID = (int) Math.ceil(Math.random() * 1000) + 2000;
		try {
			profile = GameProfileFetcher.fetch(UUID.fromString(uuid));
		} catch (IOException e) {
			profile = new GameProfile(UUID.fromString(uuid), name);
		}
		this.location = loc;
		this.player = player;
		this.name = name;
	}
	
	/** spawns this npc */
	public void spawn(){
		if(player != null){
			this.spawn(location, player);
		}else{
			for(Player p : Bukkit.getOnlinePlayers()){
				this.spawn(location, p);
			}
		}
	}
	
	public boolean isAlive() {
		return isAlive;
	}

	/** spawns this npc at location location */
	public void spawn(final Location location, final Player p){
		isAlive = true;
		if(!p.getLocation().getWorld().getName().equals(location.getWorld().getName()))
			return;
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(location != null)
					NPC.this.location = location.clone();
				PacketContainer wrap = new PacketContainer(PacketType.Play.Server.NAMED_ENTITY_SPAWN);
				wrap.getIntegers().write(0, ID);
				wrap.getUUIDs().write(0, profile.getId());
				wrap.getIntegers().write(1, Integer.valueOf((int) (NPC.this.location.getX() * 32D)));
				wrap.getIntegers().write(2, Integer.valueOf((int) (NPC.this.location.getY() * 32D)));
				wrap.getIntegers().write(3, Integer.valueOf((int) (NPC.this.location.getZ() * 32D)));
				wrap.getBytes().write(0, (byte) (NPC.this.location.getYaw() * 256F / 260F));
				wrap.getBytes().write(1, (byte) (NPC.this.location.getPitch() * 256F / 260F));
				WrappedDataWatcher w = new WrappedDataWatcher();
				w.setObject(6, (float) 20);
				wrap.getDataWatcherModifier().write(0, w);
				
				NPC.this.sendTablistPacket();
				NPC.this.send(wrap);
			}
		}.runTask(ReplaySystem.getInstance());
	}
	
	/** tp to location loc */
	public void teleport(Location loc) {
		this.location = loc;
		PacketContainer wrap = new PacketContainer(PacketType.Play.Server.ENTITY_TELEPORT);
		wrap.getIntegers().write(0, ID);
		wrap.getIntegers().write(1, Integer.valueOf((int) (NPC.this.location.getX() * 32D)));
		wrap.getIntegers().write(2, Integer.valueOf((int) (NPC.this.location.getY() * 32D)));
		wrap.getIntegers().write(3, Integer.valueOf((int) (NPC.this.location.getZ() * 32D)));
		wrap.getBytes().write(0, (byte) (NPC.this.location.getYaw() * 256F / 260F));
		wrap.getBytes().write(0, (byte) (NPC.this.location.getPitch() * 256F / 260F));
		this.send(wrap);
	}
	
	/** let this npc sneak */
	public void sneak(){
		this.isSneaking = true;
		this.isBlocking = false;
		this.isSprinting = false;
		
		WrappedDataWatcher w = new WrappedDataWatcher();
		w.setObject(0, (byte) 2);
		w.setObject(1, (short) 0);
		w.setObject(8, (byte) 0);
		
		WrapperPlayServerEntityMetadata wrap = new WrapperPlayServerEntityMetadata();
		wrap.getHandle().getIntegers().write(0, ID);
		List<WrappedWatchableObject> ws = Lists.newArrayList();
		ws.add(new WrappedWatchableObject(0, (byte) 2));
		ws.add(new WrappedWatchableObject(1, (short) 0));
		ws.add(new WrappedWatchableObject(8, (byte) 0));
		wrap.getHandle().getWatchableCollectionModifier().write(0, ws);
		
	    this.send(wrap.getHandle());
	}

	/** resets the sprinting, sneaking [...] values */
	public void resetMovement(){
		this.isBlocking = false;
		this.isSneaking = false;
		this.isSprinting = false;
		WrapperPlayServerEntityMetadata wrap = new WrapperPlayServerEntityMetadata();
		wrap.getHandle().getIntegers().write(0, ID);
		List<WrappedWatchableObject> ws = Lists.newArrayList();
		ws.add(new WrappedWatchableObject(0, (byte) 0));
		ws.add(new WrappedWatchableObject(1, (short) 0));
		ws.add(new WrappedWatchableObject(8, (byte) 0));
		wrap.getHandle().getWatchableCollectionModifier().write(0, ws);
		
	    this.send(wrap.getHandle());
	}

	/** let this npc sprint */
	public void sprint(){
		this.isSprinting = true;
		this.isBlocking = false;
		this.isSneaking = false;
		WrapperPlayServerEntityMetadata wrap = new WrapperPlayServerEntityMetadata();
		wrap.getHandle().getIntegers().write(0, ID);
		List<WrappedWatchableObject> ws = Lists.newArrayList();
		ws.add(new WrappedWatchableObject(0, (byte) 8));
		ws.add(new WrappedWatchableObject(1, (short) 0));
		ws.add(new WrappedWatchableObject(8, (byte) 0));
		wrap.getHandle().getWatchableCollectionModifier().write(0, ws);
		
	    this.send(wrap.getHandle());
	}

	/** blocks with sword, bow or consumables */
	public void block() {
		this.isBlocking = true;
		this.isSneaking = false;
		this.isSprinting = false;
		
		WrapperPlayServerEntityMetadata wrap = new WrapperPlayServerEntityMetadata();
		wrap.getHandle().getIntegers().write(0, ID);
		List<WrappedWatchableObject> ws = Lists.newArrayList();
		ws.add(new WrappedWatchableObject(0, (byte) 0x10));
		wrap.getHandle().getWatchableCollectionModifier().write(0, ws);
		
	    this.send(wrap.getHandle());
	}
	
	public void setItemInHand(ItemStack item){
		this.handHeld = item;
		
		WrapperPlayServerEntityEquipment wrap = new WrapperPlayServerEntityEquipment();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 0);
		wrap.getHandle().getItemModifier().write(0, item);
		
		this.send(wrap.getHandle());
	}	

	public void setHelmet(ItemStack helmet){
		this.helmet = helmet;
		
		WrapperPlayServerEntityEquipment wrap = new WrapperPlayServerEntityEquipment();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 4);
		wrap.getHandle().getItemModifier().write(0, helmet);
		
		this.send(wrap.getHandle());
	}
	
	public void setChestplate(ItemStack chestplate){
		this.chestplate = chestplate;
		WrapperPlayServerEntityEquipment wrap = new WrapperPlayServerEntityEquipment();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 3);
		wrap.getHandle().getItemModifier().write(0, chestplate);
		
		this.send(wrap.getHandle());
	}
	
	public void setLeggings(ItemStack leggins){
		this.leggins = leggins;
		WrapperPlayServerEntityEquipment wrap = new WrapperPlayServerEntityEquipment();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 2);
		wrap.getHandle().getItemModifier().write(0, leggins);
		
		this.send(wrap.getHandle());
	}
	
	public void setBoots(ItemStack boots){
		this.boots = boots;
		WrapperPlayServerEntityEquipment wrap = new WrapperPlayServerEntityEquipment();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 1);
		wrap.getHandle().getItemModifier().write(0, boots);
		
		this.send(wrap.getHandle());
	}
	
	/** sends a damage animation to all players */
	public void damageAnimation(){
		
		WrapperPlayServerAnimation wrap = new WrapperPlayServerAnimation();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 1);
		this.send(wrap.getHandle());
	    if(player != null){
	    	player.playSound(this.location, Sound.HURT_FLESH, 1, 1);
	    }
		else{
			for(Player p : Bukkit.getOnlinePlayers())
				p.playSound(this.location, Sound.HURT_FLESH, 1, 1);
		}
	}
	
	/** moves the npc with walking animation */
	public void move(double x, double y, double z, float yaw, float pitch){
		WrapperPlayServerRelEntityMoveLook wrap = new WrapperPlayServerRelEntityMoveLook();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getBytes().write(0, (byte) toFxdPnt(x));
		wrap.getHandle().getBytes().write(1, (byte) toFxdPnt(y));
		wrap.getHandle().getBytes().write(2, (byte) toFxdPnt(z));
		wrap.getHandle().getBytes().write(3, (byte) toAngle(yaw));
		wrap.getHandle().getBytes().write(4, (byte) toAngle(pitch));
		wrap.getHandle().getBooleans().write(0, true);
		this.send(wrap.getHandle());
		
		this.location.add(toFxdPnt(x) / 32D, toFxdPnt(y) / 32D, toFxdPnt(z) / 32D);
		this.location.setYaw(yaw);
		this.location.setPitch(pitch);
	}
	
	/** moves the npc with walking animation */
	public void move(double x, double y, double z){
		WrapperPlayServerRelEntityMoveLook wrap = new WrapperPlayServerRelEntityMoveLook();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getBytes().write(0, (byte) toFxdPnt(x));
		wrap.getHandle().getBytes().write(1, (byte) toFxdPnt(y));
		wrap.getHandle().getBytes().write(2, (byte) toFxdPnt(z));
		wrap.getHandle().getBooleans().write(0, true);
		this.send(wrap.getHandle());
		
		this.location.add(toFxdPnt(x) / 32D, toFxdPnt(y) / 32D, toFxdPnt(z) / 32D);
	}
	
	private int toFxdPnt(double value){
		return (int) Math.floor(value * 32.0D);
	}
	
	public Location getLocation(){
		return this.location;
	}
	
	/** changes players head rotation */
	public void look(float yaw, float pitch){
		WrapperPlayServerEntityHeadRotation headRot = new WrapperPlayServerEntityHeadRotation();
		headRot.getHandle().getIntegers().write(0, ID);
		headRot.getHandle().getBytes().write(0, toAngle(yaw));
		this.send(headRot.getHandle());
		
		WrapperPlayServerEntityLook look = new WrapperPlayServerEntityLook();
		look.getHandle().getIntegers().write(0, ID);
		look.getHandle().getBytes().write(3, toAngle(yaw));
		look.getHandle().getBytes().write(4, toAngle(pitch));
		this.send(look.getHandle());
		this.location.setYaw(yaw);
		this.location.setPitch(pitch);
	}
	
	private byte toAngle(float value){
		return (byte) ((int) (value * 256.0F / 360.0F));
	}
	
	/** adds the npc to the target's tablist */
	private void sendTablistPacket(){
		WrapperPlayServerPlayerInfo info = new WrapperPlayServerPlayerInfo();
		List<com.comphenix.protocol.wrappers.PlayerInfoData> data = Lists.newArrayList();
		data.add(new com.comphenix.protocol.wrappers.PlayerInfoData(new WrappedGameProfile(profile.getId(),  profile.getName()), 1, NativeGameMode.NOT_SET, WrappedChatComponent.fromText(profile.getName())));
		info.getHandle().getPlayerInfoDataLists().write(0, data);
		info.getHandle().getPlayerInfoAction().write(0, PlayerInfoAction.ADD_PLAYER);
		this.send(info.getHandle());
	}
	/** removes the npc from the target's tablist */
	private void removeFromTablist(){
		if(player != null){
			boolean isOnline = false;
			for(Player p : Bukkit.getOnlinePlayers()){
				if(this.getName().equals(p.getName()))
						isOnline = true;
			}
			if(isOnline)
				return;
		}
		
		WrapperPlayServerPlayerInfo info = new WrapperPlayServerPlayerInfo();
		List<com.comphenix.protocol.wrappers.PlayerInfoData> data = Lists.newArrayList();
		data.add(new com.comphenix.protocol.wrappers.PlayerInfoData(PacketUtil.convertIntoWrapper(profile), 0, NativeGameMode.NOT_SET, null));
		info.getHandle().getPlayerInfoDataLists().write(0, data);
		info.getHandle().getPlayerInfoAction().write(0, PlayerInfoAction.REMOVE_PLAYER);
		this.send(info.getHandle());
	}
	
	/** kills this npc */
	public void deSpawn(){
		isAlive = false;
		WrapperPlayServerEntityDestroy wrap = new WrapperPlayServerEntityDestroy();
		wrap.getHandle().getIntegerArrays().write(0, new int[] { ID });
		this.send(wrap.getHandle());
		this.removeFromTablist();
	}
	
	/** plays the item use animation (hit or place blocks...) */
	public void swingArm(){
		WrapperPlayServerAnimation wrap = new WrapperPlayServerAnimation();
		wrap.getHandle().getIntegers().write(0, ID);
		wrap.getHandle().getIntegers().write(1, 0);
		this.send(wrap.getHandle());
	}
	
	private void send(PacketContainer wrap){
		if(player != null)
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, wrap);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		else
			for(Player p : Bukkit.getOnlinePlayers()){
				try {
					ProtocolLibrary.getProtocolManager().sendServerPacket(p, wrap);
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
	}
	
	public String getName(){
		return this.name;
	}
	public boolean isSneaking() {
		return isSneaking;
	}
	public boolean isSprinting() {
		return isSprinting;
	}
	public boolean isBlocking() {
		return isBlocking;
	}
	public ItemStack getItemInHand(){
		return this.handHeld;
	}
	public ItemStack[] getArmorContents(){
		return new ItemStack[]{ helmet, chestplate, leggins, boots };
	}
}
