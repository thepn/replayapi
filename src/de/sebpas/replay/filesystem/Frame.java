package de.sebpas.replay.filesystem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Frame implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 821998681937412840L;
	
	private List<FrameAction> actions;
	private long tick;
	
	public Frame(long tick){
		actions = new ArrayList<FrameAction>();
		this.tick = tick;
	}

	public List<FrameAction> getActions() {
		return actions;
	}

	public long getTick() {
		return tick;
	}
	
	public void addAction(FrameAction a){
		actions.add(a);
	}
}
