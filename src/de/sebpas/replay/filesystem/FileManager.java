package de.sebpas.replay.filesystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.bukkit.Bukkit;

import de.sebpas.replay.ReplaySystem;
import de.sebpas.replay.filesystem.serializable.SerializedMetaData;

public class FileManager {
	private String currentFile;
	private ZipFile replayFile;
	private File tempFile;
	private File metaData;
	
	private FileOutputStream fileOut;
	private ObjectOutputStream objectOut;
	
	private String name;
	
	public FileManager(){}
	
	/**
	 * creates a new replay (zip) file which will contain all entries / recordingfiles
	 */
	public void createNewFile(){
		this.name = this.getRandom(8);
		
		this.replayFile = new ZipFile(name);
		this.createTempFile(name);
		metaData = new File(tempFile.getParent(), name + ".rmeta");
		try {
			metaData.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			this.replayFile.saveWorld(Bukkit.getWorlds().get(0));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * saves the replay file and deletes all temporary files
	 * @return true if everything is okay
	 */
	public boolean saveReplayFile(){
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(metaData));
			out.writeObject(new SerializedMetaData(ReplaySystem.getInstance().getHandledTicks(), new String[]{ "world" }));
			out.flush();
			out.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if(fileOut != null){
			try {
				fileOut.flush();
				fileOut.close();
			} catch (IOException e) {
				e.printStackTrace();
				//TODO player warning
			}
		}
		
		objectOut = null;
		fileOut = null;
		
		this.replayFile.save();
		try {
			this.replayFile.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * creates a temp replay file which will be stored in the .trpl file
	 */
	private void createTempFile(String name){
		if(replayFile == null)
			createNewFile();
		tempFile = new File(this.replayFile.getTmpFolder(), name + ".trpl");
		if(!tempFile.exists()){
			try {
				tempFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				
				//TODO player warning
			}
		}
	}
	
	
	public ZipFile openReplayFile(String name){
		ZipFile f = new ZipFile(name);
		return f;
	}
	
	public void reset(){
		if(tempFile != null && tempFile.exists()){
			try {
				Files.delete(tempFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * appends a packet to the replay file
	 * @param frame
	 */
	public synchronized void appendFrame(Frame frame) throws IOException{
		if(replayFile == null){
			createNewFile();
		}
		if(currentFile == null){
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("DD-MM-YYYY_H:m:s");
			currentFile = format.format(date);
		}
		
		if(tempFile == null)
			this.createTempFile(currentFile);
		
        try {
        	if(fileOut == null)
        		fileOut = new FileOutputStream(tempFile);
        	
        	
        	if(objectOut == null)
        		objectOut = new ObjectOutputStream(fileOut);
        	
        	objectOut.writeObject(frame);
            objectOut.reset();
        } catch (IOException e) {
        	//TODO player warning
            System.err.println(e.getMessage());
        }
	}
	
	/**
	 * @return a random generated string
	 */
	private String getRandom(int length){
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890123456789---".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) { // "i < 6" : 6 = random code lenght
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }

	public String getName() {
		return name;
	}
}
