package de.sebpas.replay.filesystem;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.bukkit.World;

import com.google.common.io.Closeables;

import de.sebpas.replay.filesystem.serializable.SerializedMetaData;

public class ZipFile {

    private final File theFile;
    private final File tmpFiles;

    private final Map<String, OutputStream> outputStreams = new HashMap<>();
    private final Map<String, File> changedEntries = new HashMap<>();
    private final Set<String> removedEntries = new HashSet<>();

    private java.util.zip.ZipFile zipFile;
    private final String name;
    
    private long maxTicks = -1;
    private String[] worlds;
    
    public ZipFile(String name){
    	this.name = name;
    	System.out.println("Loading plugins/Replays/" + name + ".rpl");
    	theFile = new File("plugins/Replays/", name + ".rpl");
    	tmpFiles = new File("plugins/Replays/", name + "_tmp");
    	if(theFile.exists()){
    		try {
    			zipFile = new java.util.zip.ZipFile(theFile);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}else{
        	tmpFiles.mkdir();
    		try {
    			theFile.createNewFile();
    			
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    	
    }

    /**
     * extracts the worlds from the zip file
     * @return an array of all worlds
     */
    public String[] unboxWorlds(){
    	if(worlds != null)
    		return worlds;
		try{
			ObjectInputStream in = new ObjectInputStream(getMetaFile());
			Object o = in.readObject();
			if(o instanceof SerializedMetaData){
				SerializedMetaData m = (SerializedMetaData) o;
				worlds = m.getSavedWorlds();
			}
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}

		if(worlds != null){
			if(theFile != null){
				if(zipFile == null)
					try {
						zipFile = new java.util.zip.ZipFile(theFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
			}

			try {
				for(ZipEntry e : Collections.list(zipFile.entries())){
					for(String world : worlds) {
						if (e.getName().contains(world)) {
							if(e.getName().contains("session.lock") || e.getName().contains("stats") || e.getName().contains("uid.dat"))
								continue;
							File toCopy = new File("replay-" + this.name + "-" + world);
							if (!toCopy.exists())
								toCopy.mkdirs();
							if(e.isDirectory()){
								File dir = new File(e.getName().replace("world", "").substring(1));
								if (!dir.exists())
									dir.mkdirs();
							}else{
								File extract = new File(toCopy, e.getName().replace(world, "").substring(1));
								 createFileInFolder(extract);
								try {
									extractFile(zipFile.getInputStream(e), extract.getAbsolutePath());
								} catch (IOException e1) {
									e1.printStackTrace();
									return null;
								}
							}
						}
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return worlds;
	}

    /**
     * creates a file with its parents
     * @param f
     * @throws IOException
     */
	private static void createFileInFolder(File f) throws IOException{
		if(f.getParentFile() == null){
			if(f.isDirectory())
				f.mkdirs();
			else
				f.createNewFile();
			return;
		}
		if(!f.getParentFile().exists())
			createFileInFolder(f.getParentFile());
		if(!f.getName().contains("."))
			f.mkdirs();
		else
			f.createNewFile();
	}

	/**
	 * Extracts a zip entry (file entry)
	 * @param zipIn
	 * @param filePath
	 * @throws IOException
	 */
	private void extractFile(InputStream zipIn, String filePath) throws IOException {
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
		byte[] bytesIn = new byte[4096];
		int read = 0;
		while ((read = zipIn.read(bytesIn)) > 0) {
			bos.write(bytesIn, 0, read);
		}
		bos.flush();
		bos.close();
	}
    
    /**
     * saves the world to the replay file
     * @param arg (the world which should be saved)
     * @throws FileNotFoundException, IOException
     */
    public void saveWorld(World arg) throws FileNotFoundException, IOException{
    	arg.save();
    	File worldFile = new File(arg.getName());
    	if(!worldFile.isDirectory() || !worldFile.exists())
    		throw new FileNotFoundException(worldFile.getAbsolutePath());
    	
    	if(tmpFiles == null)
    		throw new NullPointerException();
    	
    	File toCopy = new File(tmpFiles, "world");
    	toCopy.mkdirs();
    	
    	FileUtils.copyDirectory(worldFile, toCopy);
    }
    
    /**
     * saves the replay as .rpl (zip file)
     * @param out
     * @param folder
     * @throws IOException
     */
    private synchronized void saveInZipFile(ZipOutputStream out, File folder) throws IOException{
    	for(File e : folder.listFiles()){
    		System.out.println("Saving " + e.getName());
			if(!e.exists()){
				System.out.println(e.getAbsolutePath() + " does not exist.");
				continue;
			}
			
			if(e.isDirectory()){
				this.saveInZipFile(out, e);
				continue;
			}
			
			FileInputStream in = new FileInputStream(e);
			ZipEntry entry = new ZipEntry(e.getPath().substring(tmpFiles.getPath().length() + 1));
			out.putNextEntry(entry);
			byte[] buffer = new byte[1024];
			int len;
			
			while((len = in.read(buffer)) > 0){
				out.write(buffer, 0, len);
			}
			out.closeEntry();
			in.close();
		}
    }
    
    /**
     * saves the replay
     */
    public void save(){
    	System.out.println("Saving to " + theFile.getAbsolutePath());
    	
    	try {
			if(tmpFiles != null && theFile != null && tmpFiles.listFiles() != null){
				ZipOutputStream out = new ZipOutputStream(new FileOutputStream(theFile));
				this.saveInZipFile(out, tmpFiles);
				out.flush();
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    	try {
			this.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * @return latest tick which is saved in the replay file
     */
    public long getMaxTicks(){
    	if(maxTicks == -1){
    		long l = 20;
    		try{
    			ObjectInputStream in = new ObjectInputStream(getMetaFile());
        		Object o = in.readObject();
        		if(o instanceof SerializedMetaData){
        			SerializedMetaData m = (SerializedMetaData) o;
        			l = m.getMaxTicks();
        		}
        		in.close();
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		return l;
    	}
    	
    	return maxTicks;
    }
    
    /**
     * @return the inputstream to read the meta file (.rmeta)
     * @throws IOException
     */
    public InputStream getMetaFile() throws IOException{
    	if(theFile != null){
    		if(zipFile == null)
        		zipFile = new java.util.zip.ZipFile(theFile);
    	}
    	
    	for(ZipEntry e : Collections.list(zipFile.entries())){
    		if(!e.isDirectory() && e.getName().contains(".rmeta")){
    			return zipFile.getInputStream(e);
    		}
    	}
    	return null;
    }
    
    
    /**
     * @return the inputstream to read a .trpl file which the .rpl / Zip file contains
     * @throws IOException
     */
    public InputStream getReplayFile() throws IOException{
    	if(theFile != null){
    		if(zipFile == null)
        		zipFile = new java.util.zip.ZipFile(theFile);
    	}
    	
    	for(ZipEntry e : Collections.list(zipFile.entries())){
    		if(!e.isDirectory() && e.getName().contains(".trpl")){
    			return zipFile.getInputStream(e);
    		}
    	}
    	return null;
    }
    
    /**
     * closes the replay file
     * @throws IOException
     */
    public void close() throws IOException {
        if (zipFile != null) {
            zipFile.close();
        }
        for (OutputStream out : outputStreams.values()) {
            Closeables.close(out, true);
        }
        outputStreams.clear();

        changedEntries.clear();
        removedEntries.clear();
        delete(tmpFiles);
    }

    /**
     * deletets file f with its children
     * @param file
     * @throws IOException
     */
    private void delete(File file) throws IOException {
        File[] children = file.listFiles();
        if (children != null) {
            for (File child : children) {
                delete(child);
            }
        }
        Files.deleteIfExists(file.toPath());
    }
    
    public final String getName(){
    	return name;
    }
    
    public File getTmpFolder(){
    	return tmpFiles;
    }
}
