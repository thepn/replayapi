package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;
import java.util.UUID;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;

public class SerializedGameProfile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3554589545512569357L;
	
	private UUID uuid;
	private String name;
	
	public SerializedGameProfile(UUID uuid, String name){
		this.uuid = uuid;
		this.name = name;
	}
	
	public SerializedGameProfile(GameProfile gp){
		this.name = gp.getName();
		this.uuid = gp.getId();
	}

	public GameProfile toGameProfile(){
		return new GameProfile(uuid, name);
	}
	
	public WrappedGameProfile toWrappedGameProfile(){
		return new WrappedGameProfile(uuid, name);
	}
	
	public UUID getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}
	
}
