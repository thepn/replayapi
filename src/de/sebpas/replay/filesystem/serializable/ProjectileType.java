package de.sebpas.replay.filesystem.serializable;

public enum ProjectileType {
	ARROW,
	EGG,
	SNOWBALL,
	ENDERPEARL,
	EXPBOTTLE,
	POTION;
}
