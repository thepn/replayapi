package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;

import com.comphenix.packetwrapper.WrapperPlayServerCustomSoundEffect;
import com.comphenix.protocol.wrappers.EnumWrappers.SoundCategory;

public class SerializedSoundEffect implements Serializable{

	/**
	 * serial id
	 */
	private static final long serialVersionUID = -8049025753154764497L;
	
	private int x;
	private int y;
	private int z;
	
	private float volume;
	private float pitch;
	
	private String soundname;
	private SoundCategory category;
	
	public SerializedSoundEffect(WrapperPlayServerCustomSoundEffect packet){
		this.x = packet.getX();
		this.y = packet.getY();
		this.z = packet.getZ();
		this.volume = packet.getVolume();
		this.pitch = packet.getPitch();
		this.soundname = packet.getSoundName();
		this.category = packet.getSoundCategory();
	}
	
	public WrapperPlayServerCustomSoundEffect toPacket(){
		WrapperPlayServerCustomSoundEffect packet = new WrapperPlayServerCustomSoundEffect();
		packet.setX(x);
		packet.setY(y);
		packet.setZ(z);
		packet.setVolume(volume);
		packet.setPitch(pitch);
		packet.setSoundName(soundname);
		packet.setSoundCategory(category);
		return packet;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public float getVolume() {
		return volume;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public String getSoundname() {
		return soundname;
	}
	
	public SoundCategory getCategory() {
		return category;
	}
}
