package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;

import org.bukkit.enchantments.Enchantment;

public class SerializedEnchantment implements Serializable{
	  private static final long serialVersionUID = -7278634840659843907L;
	  private final int ID;

	  @SuppressWarnings("deprecation")
	public SerializedEnchantment(Enchantment enchantment)
	  {
	    this.ID = enchantment.getId();
	  }

	  @SuppressWarnings("deprecation")
	public Enchantment unbox()
	  {
	    return Enchantment.getById(this.ID);
	  }
}
