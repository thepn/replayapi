package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;

import de.sebpas.replay.RePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class SerializedLocation implements Serializable{
	private static final long serialVersionUID = -3568271653305872541L;
	private double x;
	private double y;
	private double z;
	private float yaw;
	private float pitch;
	private String world;
	
	public SerializedLocation(Location location){
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
		this.world = location.getWorld().getName();
	}

	public Location getLocation(RePlayer player) {
		return new Location(Bukkit.getWorld("replay-" + player.getId() + "-" + this.world), this.x, this.y, this.z, this.yaw, this.pitch);
	} 
}
