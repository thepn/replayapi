package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;

import org.bukkit.Color;

public class SerializedColor implements Serializable{
	  private static final long serialVersionUID = 3970770695826244515L;
	  private int r;
	  private int g;
	  private int b;

	  public SerializedColor(int r, int g, int b)
	  {
	    this.r = r;
	    this.g = g;
	    this.b = b;
	  }

	  public Color unbox() {
	    Color color = Color.fromRGB(this.r, this.g, this.b);
	    return color;
	  }
}
