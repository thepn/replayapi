package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class SerializedItem implements Serializable{
	  private static final long serialVersionUID = 1911110162854702366L;
	  private final int TYPE;
	  private final int AMOUNT;
	  private final short DAMAGE;
	  private final byte DATA;
	  private final boolean IS_NULL;
	  private final SerializedColor COLOR;
	  private Map<SerializedEnchantment, Integer> enchants;

	  @SuppressWarnings("deprecation")
	public SerializedItem(ItemStack itemStack, Color color)
	  {
	    this.IS_NULL = (itemStack == null);
	    if (this.IS_NULL) {
	      this.TYPE = 0;
	      this.AMOUNT = 0;
	      this.DAMAGE = 0;
	      this.DATA = 0;
	      this.enchants = null;
	      this.COLOR = null;
	    } else {
	      this.TYPE = itemStack.getTypeId();
	      this.AMOUNT = itemStack.getAmount();
	      this.DAMAGE = itemStack.getDurability();
	      this.DATA = itemStack.getData().getData();
	      this.COLOR = new SerializedColor(color.getRed(), color.getGreen(), color.getBlue());

	      Map<SerializedEnchantment, Integer> map = new HashMap<SerializedEnchantment, Integer>();

	      Map<Enchantment, Integer> enchantments = itemStack.getEnchantments();
	      for (Enchantment enchantment : enchantments.keySet()) {
	        map.put(new SerializedEnchantment(enchantment), enchantments.get(enchantment));
	      }
	      this.enchants = map;
	    }
	  }

	  @SuppressWarnings("deprecation")
	public SerializedItem(ItemStack itemStack) {
	    this.IS_NULL = (itemStack == null);
	    if (this.IS_NULL) {
	      this.TYPE = 0;
	      this.AMOUNT = 0;
	      this.DAMAGE = 0;
	      this.DATA = 0;
	      this.COLOR = null;
	      this.enchants = null;
	    } else {
	      this.TYPE = itemStack.getTypeId();
	      this.AMOUNT = itemStack.getAmount();
	      this.DAMAGE = itemStack.getDurability();
	      this.DATA = itemStack.getData().getData();
	      this.COLOR = null;

	      Map<SerializedEnchantment, Integer> map = new HashMap<SerializedEnchantment, Integer>();

	      Map<Enchantment, Integer> enchantments = itemStack.getEnchantments();
	      for (Enchantment enchantment : enchantments.keySet()) {
	        map.put(new SerializedEnchantment(enchantment), enchantments.get(enchantment));
	      }
	      this.enchants = map;
	    }
	  }

	  @SuppressWarnings("deprecation")
	public ItemStack unbox()
	  {
	    if (this.IS_NULL) {
	      return null;
	    }

	    if (this.enchants == null) {
	      this.enchants = new HashMap<SerializedEnchantment, Integer>();
	    }

	    ItemStack item = new ItemStack(this.TYPE, this.AMOUNT, this.DAMAGE, Byte.valueOf(this.DATA));

	    if ((item.getType() == Material.LEATHER_HELMET) || (item.getType() == Material.LEATHER_CHESTPLATE) || (item.getType() == Material.LEATHER_LEGGINGS) || (item.getType() == Material.LEATHER_BOOTS)) {
	      LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta)item.getItemMeta();
	      leatherArmorMeta.setColor(this.COLOR.unbox());
	      item.setItemMeta(leatherArmorMeta);
	    }

	    Map<Enchantment, Integer> map = new HashMap<Enchantment, Integer>();
	    for (SerializedEnchantment enchantment : this.enchants.keySet()) {
	      map.put(enchantment.unbox(), this.enchants.get(enchantment));
	    }
	    item.addUnsafeEnchantments(map);

	    return item;
	  }
}
