package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;

public class SerializedMetaData implements Serializable{
	
	/**
	 * serial id
	 */
	private static final long serialVersionUID = 5196078788168277883L;
	
	private long maxTicks;
	private String[] savedWorlds = null;

	public SerializedMetaData(long maxTicks, String[] savedWorlds){
		this.maxTicks = maxTicks;
		this.savedWorlds = savedWorlds;
	}
	
	public long getMaxTicks() {
		return maxTicks;
	}

	public String[] getSavedWorlds(){
		return savedWorlds;
	}
	
}
