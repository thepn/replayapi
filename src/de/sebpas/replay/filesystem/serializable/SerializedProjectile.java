package de.sebpas.replay.filesystem.serializable;

import java.io.Serializable;

public class SerializedProjectile implements Serializable{
	private static final long serialVersionUID = 5709566599096316686L;
	private double x;
	private double y;
	private double z;
	private ProjectileType type;
	private SerializedItem potionItem;

	public SerializedProjectile(double x, double y, double z, ProjectileType type, SerializedItem potionItem){
		this.x = x; 
		this.y = y; 
		this.z = z;
		this.type = type; 
		this.potionItem = potionItem;
	}

	public double getX(){
		return this.x; 
	} 
	public double getY() {
		return this.y; 
	} 
	public double getZ() {
		return this.z; 
	}

	public ProjectileType getType() {
		return this.type; 
	} 
	public SerializedItem getPotionItem() { 
		return this.potionItem;
	}
}
