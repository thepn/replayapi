package de.sebpas.replay.filesystem;

import java.io.Serializable;

import de.sebpas.replay.RePlayer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import de.sebpas.replay.filesystem.serializable.SerializedGameProfile;
import de.sebpas.replay.filesystem.serializable.SerializedItem;
import de.sebpas.replay.filesystem.serializable.SerializedLocation;
import de.sebpas.replay.filesystem.serializable.SerializedProjectile;
import de.sebpas.replay.filesystem.serializable.SerializedSoundEffect;

public class FrameAction implements Serializable {

	/**
	 * this class represents an action which happened it a parent frame
	 * 
	 * serialization identifier
	 */
	private static final long serialVersionUID = 2732867280958486120L;

	/** general */
	private String[] chatMessages;
	private SerializedGameProfile gp;

	/** moving */
	private boolean sneaking;
	private boolean sprinting;
	private boolean damaged;
	private boolean blocking;
	private boolean swing;
	private boolean isMoving = false;
	private boolean isMovedByKnockBack = false;
	private SerializedLocation location;

	private boolean dead;
	private boolean spawn;

	/** items */
	private SerializedItem iteminhand;
	private SerializedItem[] armorTopDown;
	private SerializedProjectile[] projectiles;
	private SerializedItem[] throwables;
	private SerializedItem[] despawnedItems;
	private Material changedBlocks;

	private SerializedSoundEffect[] sounds;

	public FrameAction(SerializedGameProfile gp) {
		this.gp = gp;
	}

	public FrameAction(SerializedGameProfile gp,
			SerializedProjectile[] projectiles) {
		this.projectiles = projectiles;
		this.gp = gp;
	}
	
	public FrameAction(SerializedGameProfile gp, Location location,
			boolean sprinting, boolean sneaking, SerializedItem itemInHand,
			PlayerInventory inv) {
		this.location = new SerializedLocation(location);
		this.gp = gp;
		this.sneaking = sneaking;
		this.sprinting = sprinting;
		this.iteminhand = itemInHand;

		this.isMoving = true;

		this.swing = false;

		this.armorTopDown = new SerializedItem[4];

		if (inv.getHelmet() != null
				&& inv.getHelmet().getType() == Material.LEATHER_HELMET) {
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) inv
					.getHelmet().getItemMeta();

			this.armorTopDown[0] = new SerializedItem(inv.getHelmet(),
					leatherArmorMeta.getColor());
		} else {
			this.armorTopDown[0] = new SerializedItem(inv.getHelmet());
		}

		if ((inv.getChestplate() != null)
				&& (inv.getChestplate().getType() == Material.LEATHER_CHESTPLATE)) {
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) inv
					.getChestplate().getItemMeta();

			this.armorTopDown[1] = new SerializedItem(inv.getChestplate(),
					leatherArmorMeta.getColor());
		} else {
			this.armorTopDown[1] = new SerializedItem(inv.getChestplate());
		}

		if ((inv.getLeggings() != null)
				&& (inv.getLeggings().getType() == Material.LEATHER_LEGGINGS)) {
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) inv
					.getLeggings().getItemMeta();

			this.armorTopDown[2] = new SerializedItem(inv.getLeggings(),
					leatherArmorMeta.getColor());
		} else {
			this.armorTopDown[2] = new SerializedItem(inv.getLeggings());
		}

		if ((inv.getBoots() != null)
				&& (inv.getBoots().getType() == Material.LEATHER_BOOTS)) {
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) inv
					.getBoots().getItemMeta();

			this.armorTopDown[3] = new SerializedItem(inv.getBoots(),
					leatherArmorMeta.getColor());
		} else {
			this.armorTopDown[3] = new SerializedItem(inv.getBoots());
		}
	}

	public void setChangedBlock(Material m) {
		this.changedBlocks = m;
	}

	public boolean isBlocking() {
		return this.blocking;
	}

	public void setBlocking(boolean blocking) {
		this.blocking = blocking;
	}

	public void setDamaged(boolean damaged) {
		this.damaged = damaged;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public boolean isDead() {
		return this.dead;
	}

	public boolean isDamaged() {
		return this.damaged;
	}

	public ItemStack getIteminhand() {
		if (this.iteminhand == null)
			return null;
		return this.iteminhand.unbox();
	}

	public String[] getChatMessages() {
		return this.chatMessages;
	}

	public Location getLocation(RePlayer player) {
		if (this.location == null)
			return new Location(Bukkit.getWorlds().get(0), 0, 100, 0);
		return this.location.getLocation(player);
	}

	public ItemStack[] getThrowables() {
		if (this.throwables == null) {
			return new ItemStack[0];
		}
		ItemStack[] items = new ItemStack[this.throwables.length];
		for (int i = 0; i < items.length; i++) {
			items[i] = this.throwables[i].unbox();
		}
		return items;
	}

	public boolean isSneaking() {
		return this.sneaking;
	}

	public boolean isSprinting() {
		return this.sprinting;
	}

	public boolean isSwinging() {
		return this.swing;
	}

	public SerializedProjectile[] getProjectiles() {
		return this.projectiles;
	}

	public void setArrows(SerializedProjectile[] projectiles) {
		this.projectiles = projectiles;
	}

	public void setHelmet(ItemStack helmet) {
		if (armorTopDown == null)
			armorTopDown = new SerializedItem[4];
		this.armorTopDown[0] = new SerializedItem(helmet);
	}

	public void setChestPlate(ItemStack chestPlate) {
		if (armorTopDown == null)
			armorTopDown = new SerializedItem[4];
		this.armorTopDown[1] = new SerializedItem(chestPlate);
	}

	public void setLeggings(ItemStack i) {
		if (armorTopDown == null)
			armorTopDown = new SerializedItem[4];
		this.armorTopDown[2] = new SerializedItem(i);
	}

	public void setBoots(ItemStack i) {
		if (armorTopDown == null)
			armorTopDown = new SerializedItem[4];
		this.armorTopDown[3] = new SerializedItem(i);
	}

	public ItemStack getHelmet() {
		if (armorTopDown == null)
			return null;
		return this.armorTopDown[0].unbox();
	}

	public ItemStack getChestPlate() {
		if (armorTopDown == null)
			return null;
		return this.armorTopDown[1].unbox();
	}

	public ItemStack getLeggings() {
		if (armorTopDown == null)
			return null;
		return this.armorTopDown[2].unbox();
	}

	public ItemStack getBoots() {
		if (armorTopDown == null)
			return null;
		return this.armorTopDown[3].unbox();
	}

	public SerializedGameProfile getGameProfile() {
		return gp;
	}

	public boolean isMoving() {
		return isMoving;
	}

	public boolean isMovedByKnockBack() {
		return isMovedByKnockBack;
	}

	public void setChatMessages(String[] chatMessages) {
		this.chatMessages = chatMessages;
	}

	public void setGp(SerializedGameProfile gp) {
		this.gp = gp;
	}

	public void setSneaking(boolean sneaking) {
		this.sneaking = sneaking;
	}

	public void setSprinting(boolean sprinting) {
		this.sprinting = sprinting;
	}

	public void setSwing(boolean swing) {
		this.swing = swing;
	}

	public void setMoving(boolean isMoving) {
		this.isMoving = isMoving;
	}

	public void setMovedByKnockBack(boolean isMovedByKnockBack) {
		this.isMovedByKnockBack = isMovedByKnockBack;
	}

	public void setLocation(SerializedLocation location) {
		this.location = location;
	}

	public void setIteminhand(SerializedItem iteminhand) {
		this.iteminhand = iteminhand;
	}

	public void setArmorTopDown(SerializedItem[] armorTopDown) {
		this.armorTopDown = armorTopDown;
	}

	public void setProjectiles(SerializedProjectile[] projectiles) {
		this.projectiles = projectiles;
	}

	public void setThrowables(SerializedItem[] throwables) {
		this.throwables = throwables;
	}

	public ItemStack[] getDespawnedItems() {
		if (despawnedItems == null)
			return new ItemStack[0];
		ItemStack[] items = new ItemStack[despawnedItems.length];
		for (int i = 0; i < items.length; i++)
			items[i] = despawnedItems[i].unbox();
		System.out.println(items.length);
		return items;
	}

	public void setDespawnedItems(SerializedItem[] despawnedItems) {
		this.despawnedItems = despawnedItems;
	}

	public SerializedSoundEffect[] getSounds() {
		return sounds;
	}

	public void setSounds(SerializedSoundEffect[] sounds) {
		this.sounds = sounds;
	}

	public boolean isSpawn() {
		return spawn;
	}

	public void setSpawn(boolean spawn) {
		this.spawn = spawn;
	}

	public Material getChangedBlocks() {
		return changedBlocks;
	}
}
