package de.sebpas.replay.recorder.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.sebpas.replay.ReplaySystem;
import de.sebpas.replay.filesystem.FrameAction;
import de.sebpas.replay.filesystem.serializable.SerializedGameProfile;
import de.sebpas.replay.filesystem.serializable.SerializedLocation;

public class SpawnDespawnListener implements Listener{
	private ReplaySystem plugin;
	
	public SpawnDespawnListener(ReplaySystem plugin){
		this.plugin = plugin;
	}
	
	/**
	 * Join and disconnect events
	 * @param e
	 */
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoinEvent(PlayerJoinEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		Player p = (Player) e.getPlayer();
		
		FrameAction action = new FrameAction(new SerializedGameProfile(p.getUniqueId(), p.getName()));
		action.setLocation(new SerializedLocation(p.getLocation().clone()));
		action.setSpawn(true);
		plugin.getRecorder().addAction(action);
	}
	@EventHandler(priority = EventPriority.LOWEST)
	public void onLeaveEvent(PlayerQuitEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		Player p = (Player) e.getPlayer();
		
		FrameAction action = new FrameAction(new SerializedGameProfile(p.getUniqueId(), p.getName()));
		action.setDead(true);
		plugin.getRecorder().addAction(action);
	}
	
	/**
	 * death and respawn events
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDieEvent(PlayerDeathEvent e){
		if(!plugin.getRecorder().isRecording() || e.getEntityType() != EntityType.PLAYER)
			return;
		Player p = (Player) e.getEntity();
		
		FrameAction action = new FrameAction(new SerializedGameProfile(p.getUniqueId(), p.getName()));
		action.setDead(true);
		action.setChatMessages(new String[]{ e.getDeathMessage() });
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onRespawnEvent(PlayerRespawnEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		Player p = (Player) e.getPlayer();
		
		FrameAction action = new FrameAction(new SerializedGameProfile(p.getUniqueId(), p.getName()));
		action.setLocation(new SerializedLocation(p.getLocation().clone()));
		action.setSpawn(true);
		plugin.getRecorder().addAction(action);
	}
}
