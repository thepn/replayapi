package de.sebpas.replay.recorder.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;

import de.sebpas.replay.ReplaySystem;
import de.sebpas.replay.filesystem.FrameAction;
import de.sebpas.replay.filesystem.serializable.SerializedGameProfile;
import de.sebpas.replay.filesystem.serializable.SerializedItem;

public class MoveListener implements Listener {
	/**
	 * this listener records moving related events
	 */
	
	private ReplaySystem plugin;

	public MoveListener(ReplaySystem plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if (plugin.getRecorder().isRecording())
			plugin.getRecorder().addAction(
					new FrameAction(new SerializedGameProfile(e.getPlayer()
							.getUniqueId(), e.getPlayer().getName()), e
							.getPlayer().getLocation(), e.getPlayer()
							.isSprinting(), e.getPlayer().isSneaking(),
							new SerializedItem(e.getPlayer().getItemInHand()),
							e.getPlayer().getInventory()));
	}

	@EventHandler
	public void onToggleSneak(PlayerToggleSneakEvent e) {
		if (plugin.getRecorder().isRecording()) {
			FrameAction action = new FrameAction(new SerializedGameProfile(e
					.getPlayer().getUniqueId(), e.getPlayer().getName()));
			action.setSneaking(e.isSneaking());
			action.setSprinting(e.getPlayer().isSprinting());
			plugin.getRecorder().addAction(action);
		}
	}

	@EventHandler
	public void onToggleSprint(PlayerToggleSprintEvent e){
		if(plugin.getRecorder().isRecording()){
			FrameAction action = new FrameAction(new SerializedGameProfile(e.getPlayer().getUniqueId(),  e.getPlayer().getName()));
			action.setSneaking(e.getPlayer().isSneaking());
			action.setSprinting(e.getPlayer().isSprinting());
			plugin.getRecorder().addAction(action);
		}
	}
}
