package de.sebpas.replay.recorder.listener;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import de.sebpas.replay.ReplaySystem;
import de.sebpas.replay.filesystem.FrameAction;
import de.sebpas.replay.filesystem.serializable.ProjectileType;
import de.sebpas.replay.filesystem.serializable.SerializedGameProfile;
import de.sebpas.replay.filesystem.serializable.SerializedItem;
import de.sebpas.replay.filesystem.serializable.SerializedLocation;
import de.sebpas.replay.filesystem.serializable.SerializedProjectile;

public class InteractListener implements Listener{
	/**
	 * this class is listening to events and records them
	 */
	
	private ReplaySystem plugin;
	
	public InteractListener(ReplaySystem plugin){
		this.plugin = plugin;
	}
	@EventHandler(priority = EventPriority.LOWEST)
	public void onAttack(PlayerInteractEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(new SerializedGameProfile(e.getPlayer().getUniqueId(), e.getPlayer().getName()));
		if(e.getAction() != Action.PHYSICAL){
			action.setIteminhand(new SerializedItem(e.getPlayer().getItemInHand()));
			action.setSwing(true);
		}
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(e.getItem() != null){
				action.setIteminhand(new SerializedItem(e.getPlayer().getItemInHand()));
				action.setBlocking(true);
				action.setSwing(false);
			}
		}
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockBreakEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(null);
		action.setLocation(new SerializedLocation(e.getBlock().getLocation()));
		action.setChangedBlock(Material.AIR);
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockPlaceEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(null);
		action.setLocation(new SerializedLocation(e.getBlock().getLocation()));
		action.setChangedBlock(e.getBlockPlaced().getType());
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDamage(EntityDamageEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		if(e.getEntityType() != EntityType.PLAYER)
			return;
		Player p = (Player) e.getEntity();
		if(ReplaySystem.getInstance().isAlreadyInReplay(p)){
			e.setCancelled(true);
			return;
		}
		
		FrameAction action = new FrameAction(new SerializedGameProfile(p.getUniqueId(), p.getName()));
		action.setDamaged(true);
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onItemInHandChanged(PlayerItemHeldEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(new SerializedGameProfile(e.getPlayer().getUniqueId(), e.getPlayer().getName()));
		action.setIteminhand(new SerializedItem(e.getPlayer().getItemInHand()));
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onArmorChanged(InventoryClickEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(new SerializedGameProfile(e.getWhoClicked().getUniqueId(), e.getWhoClicked().getName()));
		action.setHelmet(e.getWhoClicked().getInventory().getHelmet());
		action.setChestPlate(e.getWhoClicked().getInventory().getChestplate());
		action.setLeggings(e.getWhoClicked().getInventory().getLeggings());
		action.setBoots(e.getWhoClicked().getInventory().getBoots());
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onProjectile(ProjectileLaunchEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(null);
		ProjectileType type = ProjectileType.ARROW;
		if(e.getEntityType() == EntityType.EGG)
			type = ProjectileType.EGG;
		else if(e.getEntityType() == EntityType.ENDER_PEARL)
			type = ProjectileType.ENDERPEARL;
		else if(e.getEntityType() == EntityType.THROWN_EXP_BOTTLE)
			type = ProjectileType.EXPBOTTLE;
		else if(e.getEntityType() == EntityType.SNOWBALL)
			type = ProjectileType.SNOWBALL;
		else if(type != ProjectileType.ARROW)
			type = ProjectileType.POTION;
		SerializedItem item = null;
		
		if(type == ProjectileType.POTION){
			item = new SerializedItem(((ThrownPotion) e.getEntity()).getItem());
		}
		
		action.setProjectiles(new SerializedProjectile[] { new SerializedProjectile(e.getEntity().getVelocity().getX(), e.getEntity().getVelocity().getY(), e.getEntity().getVelocity().getZ(), type, item) });
		action.setLocation(new SerializedLocation(e.getEntity().getLocation()));
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onChatMsg(AsyncPlayerChatEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(new SerializedGameProfile(e.getPlayer().getUniqueId(), e.getPlayer().getName()));
		
		String msg = e.getFormat().replace("%MSG%", "%2$s");
		msg = msg.replace("%PLAYER%", "%1$s");
		
		action.setChatMessages(new String[] { msg });
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler
	public void onCollect(PlayerPickupItemEvent e){
		Player p = (Player) e.getPlayer();
		if(ReplaySystem.getInstance().isAlreadyInReplay(p)){
			e.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = (Player) e.getPlayer();
		if(ReplaySystem.getInstance().isAlreadyInReplay(p)){
			e.setCancelled(true);
			return;
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDeSpawn(ItemDespawnEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(null);
		action.setDespawnedItems(new SerializedItem[] { new SerializedItem(e.getEntity().getItemStack().clone()) });
		action.setLocation(new SerializedLocation(e.getLocation().clone()));
		plugin.getRecorder().addAction(action);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDrop(ItemSpawnEvent e){
		if(!plugin.getRecorder().isRecording())
			return;
		
		FrameAction action = new FrameAction(null);
		action.setThrowables(new SerializedItem[] { new SerializedItem(e.getEntity().getItemStack().clone()) });
		action.setLocation(new SerializedLocation(e.getLocation().clone()));
		plugin.getRecorder().addAction(action);
	}
}
