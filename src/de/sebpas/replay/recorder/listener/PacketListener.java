package de.sebpas.replay.recorder.listener;

import com.comphenix.packetwrapper.WrapperPlayServerCustomSoundEffect;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import de.sebpas.replay.ReplaySystem;
import de.sebpas.replay.filesystem.FrameAction;
import de.sebpas.replay.filesystem.serializable.SerializedGameProfile;
import de.sebpas.replay.filesystem.serializable.SerializedSoundEffect;

public class PacketListener {
	
	public static void listenToPackets(ReplaySystem plugin){
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(ReplaySystem.getInstance(), PacketType.Play.Server.CUSTOM_SOUND_EFFECT){
			@Override
			public void onPacketSending(PacketEvent e) {
				if(ReplaySystem.getInstance().getRecorder().isRecording()){
					if(e.getPacketType() == PacketType.Play.Server.CUSTOM_SOUND_EFFECT){
						WrapperPlayServerCustomSoundEffect packet = new WrapperPlayServerCustomSoundEffect(e.getPacket());
						FrameAction action = new FrameAction(new SerializedGameProfile(e.getPlayer().getUniqueId(), e.getPlayer().getName()));
						action.setSounds(new SerializedSoundEffect[]{ new SerializedSoundEffect(packet) });
						ReplaySystem.getInstance().getRecorder().addAction(action);
					}
				}
				super.onPacketSending(e);
			}
		});
	}
}
