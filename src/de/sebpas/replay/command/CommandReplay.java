package de.sebpas.replay.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.sebpas.replay.RePlayer;
import de.sebpas.replay.ReplaySystem;

public class CommandReplay implements CommandExecutor{
	private ReplaySystem plugin;
	
	public CommandReplay(ReplaySystem plugin) {
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!sender.hasPermission("replay.command")){
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getPrefix() + "&cDazu bist du nicht berechtigt!"));
			return true;
		}
		
		if(cmd.getName().equalsIgnoreCase("replay")){
			if(!(sender instanceof Player)){
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "Nur f�r Spieler geeignet!"));
				return true;
			}
			if(args.length != 1 && args.length != 2){
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "Fehler: /replay <args: start stop / play / time> [file / hour:minute:second]"));
				return true;
			}
			
			if(args[0].equalsIgnoreCase("start")){
				if(plugin.getRecorder().isRecording()){
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + " &7Aufnahme bereits gestartet!"));
					return true;
				}
				
				plugin.start();
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getPrefix() + " &3Aufnahme begonnen!"));
				return true;
			}
			
			Player p = (Player) sender;
			if(args.length == 2 && args[0].equalsIgnoreCase("play")){
				System.out.println("loaded");
				if(plugin.isAlreadyInReplay(p)){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "Du befindest dich bereits in einem Replay! Du kannst dieses mit /replay stop verlassen"));
					return true;
				}
				
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getPrefix() + "&3Replay wird geladen. Bitte habe einen Moment Geduld!"));
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						RePlayer player = new RePlayer(ReplaySystem.getInstance().getFileManager().openReplayFile(args[1]), p);
						new BukkitRunnable() {
							
							@Override
							public void run() {
								if(ReplaySystem.getInstance().isAlreadyInReplay(p))
									player.start();
								else
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "Fehler: null"));
							}
						}.runTask(ReplaySystem.getInstance());					
					}
				}.runTaskAsynchronously(ReplaySystem.getInstance());
				return true;
			}
			if(args.length == 2 && args[0].equalsIgnoreCase("time")){
				String[] temp = args[1].split(":");
				int ticks = 0;
				if(temp.length == 1){
					ticks += Integer.parseInt(temp[0]) * 20;
				}
				if(temp.length == 2){
					System.out.println(temp[0] + ". " + temp[1]);
					ticks += Integer.parseInt(temp[0]) * 60 * 20 + Integer.parseInt(args[1]) * 20;
				}
				if(temp.length == 3){
					ticks += Integer.parseInt(temp[0]) * 60 * 60 * 20 + Integer.parseInt(temp[1]) * 60 * 20 + Integer.parseInt(args[2]) * 20;
				}
				RePlayer r = plugin.getPlayersRePlayer(p);
				if(r == null){
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "Du befindest nicht in einem Replay!"));
					return true;
				}
				if(r.setCurrentTick(ticks))
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getPrefix() + "&3Die Zeit wurde auf &c" + args[1] + " &3gesetzt!"));
				else
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "&cDie angegebene Zeit ist zu lang."));
			}
			if(args.length == 1 && args[0].equalsIgnoreCase("stop")){
				RePlayer r = ReplaySystem.getInstance().getPlayersRePlayer(p);
				if(r == null){
					if(plugin.getRecorder().isRecording()){
						plugin.stop();
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getPrefix() + " &3Aufnahme beendet!"));
						for(Player t : Bukkit.getOnlinePlayers()){
							TextComponent tc = new TextComponent(ChatColor.translateAlternateColorCodes('&', ReplaySystem.getInstance().getPrefix()) + "�3Das Replay wurde unter dem Namen ");
							tc.setColor(ChatColor.DARK_AQUA);
							TextComponent name = new TextComponent(ReplaySystem.getInstance().getFileManager().getName());
							name.setColor(ChatColor.YELLOW);
							name.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, ReplaySystem.getInstance().getFileManager().getName()));
							TextComponent end = new TextComponent(" �3gespeichert.");
							t.spigot().sendMessage(new TextComponent[] { tc, name, end });
						}
						return true;
					}
					
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getErrorPrefix() + "Du befindest nicht in einem Replay!"));
					return true;
				}
				r.stop();
			}
		}
		return true;
	}
	
}
